import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageToCompradorPage } from './message-to-comprador';

@NgModule({
  declarations: [
    MessageToCompradorPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageToCompradorPage),
  ],
})
export class MessageToCompradorPageModule {}
