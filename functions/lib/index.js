"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
//admin.auth().getUser().then(user => uid = user.uid).catch(error => console.error(error));
exports.onVendedorHasOnline = functions.firestore.document('usuarios/{usuarioUid}/favoritos/{favoritoId}').onUpdate(data => {
    const online = data.after.data().online;
    const token = data.after.data().token;
    const nome = data.after.data().nome;
    const hasOnline = data.after.data().hasOnline;
    const urlImg = data.after.data().urlImg;
    const payload = {
        notification: {
            title: undefined,
            body: undefined,
            sound: 'default'
        }
    };
    if (online === true && hasOnline === true) {
        payload.notification.title = `${nome} esta online`;
        payload.notification.body = `Acesse seu menu de favoritos`;
        admin.messaging().sendToDevice(token, payload).then(() => console.log('deu certo notification hasOnline'))
            .catch((error) => console.error(error));
    }
    return Promise.resolve();
});
exports.notificationCreationMessage = functions.firestore.document(`usuarios/{usuarioUid}/{messagesUid}/{messageId}`).onCreate(data => {
    const payload = {
        notification: {
            title: undefined,
            body: 'Acesse o menu mensagens.',
            sound: 'default'
        }
    };
    const nome = data.data().nome;
    const token = data.data().token;
    payload.notification.title = `${nome} escreveu uma mensagem.`;
    admin.messaging().sendToDevice(token, payload).then(() => {
        console.log('deu certo creation message');
        console.log(`${nome}- ${token}`);
    })
        .catch((error) => console.error(error));
    return Promise.resolve();
});
//# sourceMappingURL=index.js.map