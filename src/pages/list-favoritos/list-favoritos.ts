import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController, Platform } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { User, Notificacao, Favoritos } from '../../model/model';
import { OrderPipe } from 'ngx-order-pipe';
import { InformacoesUsuarioPage } from '../informacoes-usuario/informacoes-usuario';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { NotificadorProvider } from '../../providers/notificador/notificador';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { tap, timeInterval } from 'rxjs/operators';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { OpcoesFavoritoPage } from '../opcoes-favorito/opcoes-favorito';
import { interval } from 'rxjs/observable/interval';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { InformacoesVendaPage } from '../informacoes-venda/informacoes-venda';
import 'rxjs/add/operator/finally'
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-list-favoritos',
  templateUrl: 'list-favoritos.html',
})
export class ListFavoritosPage {
  usuario: User;
  vendedor: any;
  order: string = 'distanceValue ASC, online == true';
  orderOff: string = 'pessoa.nomeCompleto';
  favoritos: any[];
  favoritosOff: any[];
  start: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private uService: UsuarioProvider,
    private orderPipe: OrderPipe,
    public modalCtrl: ModalController,
    private localNotifi: LocalNotifications,
    private notService: NotificadorProvider,
    private toastCrl: ToastController,
    private favService: FavoritosProvider,
    private alertCtrl: AlertController,
    private platform: Platform,
    private launch: LaunchNavigator,
  ) {
    this.usuario = this.navParams.data.usuario;
    
      this.favService.getListaFavoritos().valueChanges()
      /*
      .take(1).toPromise()
      .catch(error => {
        console.error(error);
        this.toastCrl.create({ message: 'Erro ao carregar os vendedores favoritos.', duration: 4000, position: 'top' }).present();
      })
      */
      .subscribe((favoritos: Favoritos[]) => {
        
        if (favoritos.length > 0) {
          
          this.favoritos = [];
          this.favoritosOff = [];
          
          for (let index = 0; index < favoritos.length; index++) {
            const element = favoritos[index];
            this.uService.buscarDadosUsuarioAlternative(element.id)
            
            .subscribe((vend: any) => {
              
             let obj = this.favoritos.findIndex(fav => fav.id == vend.id);
             let obj2 = this.favoritosOff.findIndex(fav => fav.id == vend.id); 
             if(obj > -1 ) {
               this.favoritos.splice(obj,1);
             }
             if(obj2 > -1 ) {
             
              this.favoritosOff.splice(obj2,1);
            }
            
              let vendCalculado;
              if (vend.online == true) {
                vendCalculado = this.verDistanceMatrixIndividual(this.usuario, vend);
                console.log(vendCalculado);
                this.favoritos.push(vendCalculado);
              }
              else { this.favoritosOff.push(vend); }
            });
          }
        
          
         
          this.favoritos = this.orderPipe.transform(this.favoritos, this.order);
          this.favoritosOff = this.orderPipe.transform(this.favoritosOff, this.orderOff);
          console.log(this.favoritos);
          this.favoritos = [];
          this.favoritosOff = [];
          
        }
        console.log('emitindo?');
      })
    
    


    /*
    if (this.platform.is('android')) {
      this.uService.ouvirNotificacoes().pipe(
        tap(msg => {
          console.log(msg);
          this.toastCrl.create({
            
            message: `${msg.title}`,
            duration: 3000
          }).present();
          //this.navCtrl.push(ListFavoritosPage, {usuario: this.usuario});
        })
      ).subscribe(() => {
      });
    }
     */
  }

  carregarOsFavoritos() {

  }

  doInfinite(infiniteScroll:any) {
    console.log('doInfinite, start is currently '+this.start);
    this.start+=5;
    /*
    this.loadPeople().then(()=>{
      infiniteScroll.complete();
    });
    */
  }

  ionViewDidLoad() {

  }

  modalVendedor(id) {

    this.uService.pegarVendedorFavorito(id).subscribe(vend => {
      this.modalCtrl.create(InformacoesUsuarioPage, { usuarioVendedor: vend, usuarioComprador: this.usuario }).present()
    })
    //console.log(this.vendedor);
  }

  verDistanceMatrixIndividual(comprador: User, vend: any): any {

    const vendedor = {
      vendedor: {},
      distanceText: {},
      distanceValue: {}
    }

    let service = new google.maps.DistanceMatrixService();
    const origin = new google.maps.LatLng(comprador.localizacao.latitude, comprador.localizacao.longitude);
    const destination = new google.maps.LatLng(vend.localizacao.latitude, vend.localizacao.longitude);
    service.getDistanceMatrix({
      origins: [origin],
      destinations: [destination],
      travelMode: 'WALKING',
      unitSystem: google.maps.UnitSystem.METRIC
    }, (response, status) => {
      if (status !== 'OK') {
        alert("Erro no metodo. Erro: " + status)
        return;
      }
      else {

        //console.log(JSON.parse(JSON.stringify(response)));

        //vendedor.vendedor = vend;
        vend.distanceText = response.rows[0].elements[0].distance.text;
        vend.distanceValue = response.rows[0].elements[0].distance.value;
      }
    });
    //console.log(vendedor);
    return vend;
  }

  openOpcoesFavorito(favorito) {
    this.favService.getListaFavoritosPorId(favorito.id).get().then(dados => {
      favorito.dados = dados.docs.find(fav => fav.data().id == favorito.id).data();
      this.modalCtrl.create(OpcoesFavoritoPage, { favorito: favorito, usuario: this.usuario }).present()
    })
  }

  alertRemoverFavorito(favorito) {
    this.alertCtrl.create({
      title: `Remover da lista de favoritos`,
      message: `Deseja remover o(a) ${favorito.pessoa.nomeCompleto} ?`,
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.favService.removerDosFavoritos(favorito).then(() => this.toastCrl.create({ message: 'Removido com sucesso', duration: 4000 }).present())
              .catch(() => this.toastCrl.create({ message: 'Erro ao remover. Tente novamente', duration: 4000 }).present())
          }
        }
      ]
    }).present();

  }

  openLaunchNatigator(favorito) {
    let options: LaunchNavigatorOptions = {
      start: [this.usuario.localizacao.latitude, this.usuario.localizacao.longitude],
      app: this.launch.APP.GOOGLE_MAPS
    };

    this.launch.navigate([favorito.localizacao.latitude, favorito.localizacao.longitude], options)
      .then(
        success => console.log('Launched navigator'),
        error => alert('Error launching navigator' + error)
      );
  }

  modalVenda(favorito) {
    const vend = {vendedor: {}}; 
    vend.vendedor = favorito;
    let modal = this.modalCtrl.create(InformacoesVendaPage, {vendedor: vend, usuarioComprador: this.usuario});
    modal.present();
  }
}
