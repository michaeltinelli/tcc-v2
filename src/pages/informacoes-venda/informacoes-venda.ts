import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

@IonicPage()
@Component({
  selector: 'page-informacoes-venda',
  templateUrl: 'informacoes-venda.html',
})
export class InformacoesVendaPage {
  vendedor: any;
  usuarioComprador: any;
  results: any[]=[];
  mySlideOptions = {
    pager:true
  };
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private stService: StorageProvider,
    private callNumber: CallNumber,
    private launch: LaunchNavigator
  ) {
   
     this.vendedor = this.navParams.data.vendedor;
     this.usuarioComprador = this.navParams.data.usuarioComprador;
     console.log(this.usuarioComprador);
     console.log(this.vendedor);

     if (this.vendedor.vendedor) {
       
       this.stService.buscarImagensVendedor(this.vendedor.vendedor.id).subscribe(imgs => {
        if (imgs.length > 0) {
            this.results = imgs;
        }
      })
     } else if (this.vendedor.id) {
      this.stService.buscarImagensVendedor(this.vendedor.id).subscribe(imgs => {
        if (imgs.length > 0) {
            this.results = imgs;
        }
      })
     }
    //this.results = [];
  }

  ionViewDidLoad() {
    
  }

  ligarCelular(numero){
    this.callNumber.callNumber(numero, true)
    .catch(error => {
      alert("Erro ao telefonar"+ error)
    })
  }

  ligarCelularOpcional(numero){
    this.callNumber.callNumber(numero, true)
    .catch(error => {
      alert("Erro ao telefonar"+ error)
    })
  }

  openLaunchNatigator(localizacao) {
    let options: LaunchNavigatorOptions = {
      start: [this.usuarioComprador.localizacao.latitude,this.usuarioComprador.localizacao.longitude],
      app: this.launch.APP.GOOGLE_MAPS
    };
  
    this.launch.navigate([localizacao.latitude,localizacao.longitude], options)
    .then(
      success => console.log('Launched navigator'),
      error => alert('Error launching navigator' + error)
    );
  }  

  dismiss(){
    this.viewCtrl.dismiss();
  }
}
