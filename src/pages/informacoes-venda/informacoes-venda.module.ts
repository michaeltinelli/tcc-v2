import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacoesVendaPage } from './informacoes-venda';

@NgModule({
  declarations: [
    InformacoesVendaPage,
  ],
  imports: [
    IonicPageModule.forChild(InformacoesVendaPage),
  ],
})
export class InformacoesVendaPageModule {}
