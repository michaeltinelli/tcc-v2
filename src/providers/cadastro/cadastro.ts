import { Imagem, User } from './../../model/model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CadastroProvider {

  pathUsuarios ='usuarios';
  pathImagens = 'imagens';
  storageRef = firebase.storage().ref();

  private itemsCollection: AngularFirestoreCollection<User>;
  items: Observable<User[]>;
  
  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
  ) {

    this.itemsCollection = this.afs.collection<User>(this.pathUsuarios);
    this.items = this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(v => {
        const data = v.payload.doc.data() as User;
         data.id = v.payload.doc.id;
         return data;
      });
    });
  }

  criarUsuario(user: User) {
    return this.afh.auth.createUserWithEmailAndPassword(user.email, user.senha);
  }

  cadastrarUsuario(user: User)  {
    //this.db.object(`${this.pathUsuarios}/${user.id}`).set((JSON.parse(JSON.stringify(user))));
    return this.afs.doc(`${this.pathUsuarios}/${user.id}`).set((JSON.parse(JSON.stringify(user))));
     
  }
  
  
  

}
