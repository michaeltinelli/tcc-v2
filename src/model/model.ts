export class User {
    id?: string;
    email: string;
    senha: string;
    online: boolean;
    token?: string;

    //Entidades
    pessoa: Pessoa;
    contato?: Contato;
    localizacao?: Localizacao;
    imagem?: Imagem;
    imagemVendas?: ImagemVenda[];
    venda?: Venda;
    distance?: Distance;
    chatIdList?: string[];
    favoritos?: Array<Favoritos>;
    endereco?: Endereco;
}

export class Pessoa {
    nomeCompleto: string;
    //sobrenome: string;
    tipoPessoa: string;
    
}

export class Contato {
    celular: string;
    celular2?: string;
}

export class Localizacao {
    enderecoCompleto?: string;
    cidade?: string;
    bairro?: string;
    latitude: number;
    longitude: number;

}

//Perfil
export class Imagem {
    //id?: string;
    file?: any;
    progress?: number;
    name?: string;
    url?: string;
    fullPath?: string;
}

export class Venda {
    descricao?: string;
    palavrasChave?: string[];
    titulo?: string;
}

export class ImagemVenda {
    id?: string;
    file?: any;
    progress?: number;
    name?:  string;
    url?:   string;
    fullPath?: string;
}

export class Distance {
    idComprador: string;
    idVendedor: string;
    distance: string;
    duration: string;
}

export class Favoritos {
    id: string;
    nome: string;
    urlImg?: string;
    hasOnline?: boolean;
    hasPerto?: boolean;
    online?: boolean;
    token?: string;
}

export class Notificacao {
    id: string;
    idComprador: string;
    idVendedor: string;
    hasOnline?: boolean
    hasPerto?: boolean
    online?: boolean
}

export class Endereco {
    endereco?: string;
    bairro?: string;
    cidade?: string;
    estado?: string;
}

export class Device {
    id: string;
    token: string;
}

export class Message {
    id?: string;
    idRemetente?: string;
    idDestinatario?: string;
    nome: string;
    mensagem?: string;
    dataMessage?: any;
    horaMessage?: any;
    token: string; 
}

export class Messages {
    id?: string;
    idRemetente: string;
    idDestinatario: string;
    messages: Message[];
}
