
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Imagem, User, Pessoa, Contato } from '../../model/model';
import { CadastroProvider } from '../../providers/cadastro/cadastro';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { StorageProvider } from '../../providers/storage/storage';
import { storage } from 'firebase';
import { tap } from 'rxjs/operators';


@IonicPage()
@Component({
  selector: 'page-editar-dados',
  templateUrl: 'editar-dados.html',
  
})
export class EditarDadosPage {
  id = '';
  form: FormGroup;
  isVendedor: boolean;
  
  imagem: Imagem;
  usuario: User;
  
  photo: any;
  base64Image: string;
  
  error: any;
  error2: any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private usuarioService: UsuarioProvider,
    private toastCrl: ToastController,
    private camera: Camera,
    private stService: StorageProvider,
    private alertCtrl: AlertController
  ) {
    
    this.id = this.navParams.data.id;
    this.usuario = this.navParams.data.usuario;
    console.log(this.id);

    this.usuarioService.buscarDadosUsuario(this.id).subscribe((u) => {
      if(u.imagem != null){
        this.imagem = new Imagem;
        this.imagem = u.imagem;
      }
      else {
        this.imagem = null;
      }
    })
    if(this.usuario.pessoa.tipoPessoa == 'vendedor') {
        this.isVendedor = true;
    }
    else {
    this.isVendedor = false;
    }

    this.createForm(this.usuario);

    /*
    this.usuarioService.ouvirNotificacoes().pipe(
      tap(msg => {
        console.log(msg);
        this.toastCrl.create({
          
          message: `${msg.icon}  ${msg.title}`,
          duration: 3000
        }).present();
        //this.navCtrl.push(ListFavoritosPage, {usuario: this.usuario});
      })
    ).subscribe(() => {
    });
    */
  }


  ionViewDidLoad() {
    
  }

  createForm(usuario: User) {
    console.log(usuario);
   
    
   if (this.isVendedor == true) {
      
      this.form = new FormGroup({
        nomeCompleto: new FormControl(usuario.pessoa.nomeCompleto,[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(40)
        ]),
        
        celular: new FormControl(usuario.contato.celular,[
          Validators.pattern('[0-9]+'),
          Validators.minLength(10),
          Validators.required,
          Validators.maxLength(11),
          
        ]),
        celular2: new FormControl(usuario.contato.celular2,[
          Validators.pattern('[0-9]+'),
          Validators.minLength(10),
          Validators.maxLength(11)
        ]),
  
        senha: new FormControl(usuario.senha = atob(usuario.senha),[
          Validators.minLength(5),
          Validators.required,
          Validators.maxLength(15)
        ])
      });
    }
    else {
      this.form = new FormGroup({
        nomeCompleto: new FormControl(usuario.pessoa.nomeCompleto,[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(40)
        ]),
        
        senha: new FormControl(usuario.senha = atob(usuario.senha),[
          Validators.minLength(5),
          Validators.required,
          Validators.maxLength(15)
        ])
      });
    }
    
  }

  
  salvar() {
    if(this.form.valid) {
     
      this.usuario.pessoa.nomeCompleto = this.form.value.nomeCompleto;
     
      //encoda a senha
      this.usuario.senha = btoa(this.form.value.senha);
      
      this.usuario.online = false;
 
       if(this.isVendedor == true) {
      
        this.usuario.contato.celular = this.form.value.celular;
        this.usuario.contato.celular2 = this.form.value.celular2;
        this.usuario.imagem = this.imagem;
        this.usuario.pessoa.tipoPessoa = 'vendedor';
       } else {
        this.usuario.pessoa.tipoPessoa = 'comprador';
       }
       
       let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
       console.log(this.usuario);
 
       this.usuarioService.atualizarDados(this.usuario)
         .then(() => {
           toast.setMessage('Dados salvos com sucesso.');
           toast.present();
           this.navCtrl.pop();
         }) 
     }
     
   }
  
   takePhoto() {
     const options: CameraOptions = {
       quality: 50,
       destinationType: this.camera.DestinationType.DATA_URL,
       encodingType: this.camera.EncodingType.JPEG,
       mediaType: this.camera.MediaType.PICTURE,
       targetHeight: 450,
       targetWidth: 450,
       saveToPhotoAlbum: false,
       allowEdit: true
     };
     this.camera.getPicture(options)
      .then(imageData => {
        let img: Imagem;
          img = new Imagem; 
          //this.imagem = new Imagem;
          //this.photo = `data:image/jpeg;base64,${imageData}`;
          img.file = imageData;
          this.stService.salvarImagemStorage(img);
      }).catch(error => {
        this.error = error;
      })
    
   }

   deletePhoto(imagem: Imagem) {
     this.photo = imagem;
     let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
      let confirm = this.alertCtrl.create({
        title: "Você tem certeza?",
        message: "Ao confirmar a imagem será deletada <strong>Permanentemente.</strong>",
        buttons: [
          {
            text: "Não"
          }, 
          {
            text: "Sim tenho certeza!",
            handler: () => {
              this.stService.apagarFotoPerfilDB(imagem).then(() => {
                toast.setMessage('Imagem excluída.');
                this.stService.apagarFotoPerfilStorage(imagem.fullPath);
              }).catch(error => {
                toast.setMessage('Erro ao excluir.');
              })
            }
          }
        ]
      })
      confirm.present();
      toast.present();
   }


}
