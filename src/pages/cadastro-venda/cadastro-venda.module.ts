import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroVendaPage } from './cadastro-venda';

@NgModule({
  declarations: [
    CadastroVendaPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroVendaPage),
  ],
})
export class CadastroVendaPageModule {}
