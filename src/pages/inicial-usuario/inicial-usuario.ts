import { CadastroVendaPage } from './../cadastro-venda/cadastro-venda';
import { EditarDadosPage } from './../editar-dados/editar-dados';
import { UsuarioProvider } from './../../providers/usuario/usuario';
import { User, Localizacao, Distance, Favoritos, Endereco } from './../../model/model';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  MarkerCluster
 } from '@ionic-native/google-maps';
 import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Geoposition, Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs/Subscription';
import { InformacoesUsuarioPage } from '../informacoes-usuario/informacoes-usuario';
import { InformacoesVendaPage } from '../informacoes-venda/informacoes-venda';
import { OrderPipe } from 'ngx-order-pipe';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { ListFavoritosPage } from '../list-favoritos/list-favoritos';
import { Platform, AlertController } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { AngularFireAuth } from 'angularfire2/auth';
import { Firebase } from '@ionic-native/firebase';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import { MessageToCompradorPage } from '../message-to-comprador/message-to-comprador';
import { ListMessagesPage } from '../list-messages/list-messages';
import { MessagesProvider } from '../../providers/messages/messages';

declare var google: any;
declare var navigator: any;
declare var cordova;

@IonicPage()
@Component({
  selector: 'page-inicial-usuario',
  templateUrl: 'inicial-usuario.html',
})
export class InicialUsuarioPage {
  @ViewChild('map') element: ElementRef;
  map: GoogleMap;
  usuario: User;
  isVendedor: boolean;
  id = '';
  isOnline = false;
  localizacao = new Localizacao;
  endereco = new Endereco;
  error: any;
  usuariosOnline: any[];
  order: string = 'distanceValue';
  hasFav: boolean; 
  subscription: Subscription = new Subscription();
  isFavoritos: number;
  markers: Marker[]=[];
  markerVendedor: Marker;
  palavra: string = '';
  hasPalavra: boolean;
 compradorView: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private usuarioService: UsuarioProvider,
    public nativeGeocoder: NativeGeocoder,
    public geolocation: Geolocation,
    private modalCtrl: ModalController,
    private orderPipe: OrderPipe,
    private launch: LaunchNavigator,
    private platform: Platform,
    private alertCtrl: AlertController,
    private backgroundMode: BackgroundMode,
    private favService: FavoritosProvider,
    private afh: AngularFireAuth,
    private firebaseNative: Firebase,
    private toastCtrl: ToastController,
    private mService: MessagesProvider,
  ) {
     
    this.id = this.navParams.data.id;
    //console.log(this.id);
    let online = true;
    
    
    //this.subscription.add(
      this.usuarioService.buscarDadosUsuario( this.id).take(1).toPromise().then(u => {
        console.log(u);
        this.usuario = u;
        this.usuarioService.atualizarStatus(online, this.id).catch(error => console.log(error))
        .then(() => {
          if(this.usuario.pessoa.tipoPessoa == 'vendedor') {
           this.usuarioService.avisarComprador(this.id, online);
          }
        });
       

       platform.ready().then(() => {
         if(platform.is('android') || platform.is('ios')) {
           this.usuarioService.pegarToken();
           this.firebaseNative.onTokenRefresh().subscribe(token => console.log(token));
         }
        
        this.usuarioService.ouvirNotificacoes().pipe(
          tap(msg => {
            console.log(msg);
            this.toastCtrl.create({
              
              message: `${msg.title}`,
              duration: 6000
            }).present();
            //this.navCtrl.push(ListFavoritosPage, {usuario: this.usuario});
          })
        ).subscribe(() => {
        })
        
      });
      if(this.usuario.pessoa.tipoPessoa == 'vendedor') {
        this.isVendedor = true;
        //this.carregarMapa(3333);
      }
      else {
        this.isVendedor = false;
        if (this.usuario.localizacao || this.usuario.endereco) {
          this.initializeVendedores();
        }

        this.favService.getListaFavoritos().valueChanges().take(1).toPromise().then(favs => {
          this.isFavoritos = favs.length;
        })

        
      }
    }).catch(error => console.log(error));
  //  );

  }

  initializeVendedores() {
    this.subscription.add(
    
    this.usuarioService.pegarVendedoresOnline(this.usuario.endereco.cidade).subscribe(result => {
      if (result.length > 0) {
        this.usuariosOnline=[];
        result.forEach(vend => {
          //&& vend.venda.palavrasChave.find(p => p=== this.palavra)
          if (vend.venda) {
          
            let vendedor = this.verDistanceMatrixIndividual(this.usuario, vend);
            
            this.favService.getListaFavoritosPorId(vend.id).get().then(favs => {
              vendedor.hasFav =  favs.docs.find(fav => fav.data().id == vend.id) ? true : false;
             });
             
           // this.verificarListFavoritos(vend.id);
            console.log(vendedor);
            this.usuariosOnline.push(vendedor);
           
          }
         
        });
        this.usuariosOnline = this.orderPipe.transform(this.usuariosOnline, this.order)
        
      }
      else {
        let toast = this.toastCtrl.create({duration: 3000, message: "Nenhum vendedor online"});
        toast.present();
      }
    })
  );
    this.usuariosOnline=[];
  }

  
  getItems(ev: any){
    console.log(this.palavra);
    console.log(ev);
    //let val = ev.target.value;
    let val = this.palavra;
    if (val && val.trim() != '') {
     
      this.usuariosOnline = this.usuariosOnline.filter((item) => {
        return (item.vendedor.venda.titulo.toLowerCase().indexOf(val.toLowerCase()) > -1) || (item.vendedor.localizacao.enderecoCompleto.toLowerCase().indexOf(val.toLowerCase()) > -1)
        || (item.vendedor.pessoa.nomeCompleto.toLowerCase().indexOf(val.toLowerCase()) > -1) || (item.vendedor.localizacao.bairro.toLowerCase().indexOf(val.toLowerCase()) > -1) ;
      })
       if(this.usuariosOnline.length == 0) { this.hasPalavra = false; //this.toastCtrl.create({duration: 5000, message: `Nenhum resultado encontrado da busca`}).present();
      }
    }else{
      this.hasPalavra = true;
      this.initializeVendedores();
    }
    
  }


  ionViewDidEnter() {
    /*
    var onSuccess = (position) => {
      this.geocoderNativo(position);
      //this.carregarMapa(position);
      this.loadMap(position);
    };
    navigator.geolocation.getCurrentPosition(onSuccess);
   */
   
  }


  ionViewDidLoad() {
    Observable.interval(10000).subscribe(() => {
      this.geolocation.getCurrentPosition().then(data => {
        console.log(data);
        this.geocoderNativo(data);
        if (this.isVendedor == true) {
          
          if(this.map == undefined) {
            let mapOptions: GoogleMapOptions = {
              camera: {
                 target: {
                   lat: data.coords.latitude,
                   lng: data.coords.longitude
                 },
                 zoom: 18,
                 tilt: 30
                }
              };
          
              this.map = GoogleMaps.create('map', mapOptions);
            }
            else if(this.map != undefined) { 
              this.map.clear();
              /*
              this.map.setCameraTarget({
                lat: data.coords.latitude, lng: data.coords.longitude
              });
              */
              //this.map.setCameraZoom(18); 
              //this.map.setCameraTilt(30);
            } 
            
            this.loadMap(data);
            //this.map.on(GoogleMapsEvent.MAP_CLICK).toPromise().then(() => this.compradorView = undefined); 
        }
      }).catch(error => console.error(error));
    })
  
  }

  carregarMapaVendedor() {
    
  }

  geocoderNativo(data: any) {
    //-----------------------NATIVO------------------
    this.nativeGeocoder.reverseGeocode(data.coords.latitude,data.coords.longitude)
    .then((result: NativeGeocoderReverseResult) => {
      console.log(JSON.stringify(result));
      this.localizacao.latitude = data.coords.latitude;
      this.localizacao.longitude = data.coords.longitude;
      this.localizacao.cidade = result[0].locality;
      this.localizacao.enderecoCompleto = result[0].thoroughfare;

       //------------------------------google maps
    
    var geocoder = new google.maps.Geocoder;
    var latlng = {lat: data.coords.latitude, lng: data.coords.longitude};
        geocoder.geocode({'location': latlng}, (results, status) => {
          if (status === 'OK') {
            if (results[1]) {
              console.log(JSON.parse(JSON.stringify(results)));
              console.log(JSON.parse(JSON.stringify(results[0].address_components[2].long_name)));
              this.localizacao.bairro = JSON.parse(JSON.stringify(results[0].address_components[2].long_name));
              this.endereco.bairro = JSON.parse(JSON.stringify(results[0].address_components[2].long_name));
              this.endereco.estado = JSON.parse(JSON.stringify(results[0].address_components[5].long_name));
            }
          }   
        });

        this.endereco.cidade = result[0].locality;
        this.endereco.endereco = result[0].thoroughfare;
      this.usuarioService.atualizarLocalizacao(this.localizacao, this.endereco).catch(error => {
        alert(error);
      });
    })
    .catch((error: any) => {
      console.log(error)
      this.error = error;
      });
   
  }

  pageEditDados() {
    this.navCtrl.push(EditarDadosPage, {id: this.id, usuario: this.usuario});
    
  }

  pageCadastrarVenda() {
    this.navCtrl.push(CadastroVendaPage, {id: this.id, usuario: this.usuario});
    
  }
  
  openModalVendedor(usuario) {
    this.modalCtrl.create(InformacoesUsuarioPage, {usuarioVendedor: usuario, usuarioComprador: this.usuario}).present()
  }

  verDistanceMatrixIndividual(comprador: User, vend: User): any {
        
        const vendedor = {
          vendedor:  {},
          distanceText: {},
          distanceValue: {}
        }

        let service = new google.maps.DistanceMatrixService();
        const origin = new google.maps.LatLng(comprador.localizacao.latitude, comprador.localizacao.longitude);
        const destination = new google.maps.LatLng(vend.localizacao.latitude, vend.localizacao.longitude);
        service.getDistanceMatrix({
          origins: [origin],
          destinations: [destination],
          travelMode: 'WALKING',
          unitSystem: google.maps.UnitSystem.METRIC
        }, (response, status) =>  {
          if (status !== 'OK') {
            alert("Erro no metodo. Erro: " + status)
            return;
          }
          else {
            
            //console.log(JSON.parse(JSON.stringify(response)));
         
         vendedor.vendedor = vend;
         vendedor.distanceText = response.rows[0].elements[0].distance.text;
         vendedor.distanceValue =  response.rows[0].elements[0].distance.value;
        }
      }); 
      //console.log(vendedor);
      return vendedor;
  }

  modalVenda(vendedor) {
    console.log(this.usuario);
    let modal = this.modalCtrl.create(InformacoesVendaPage, {vendedor: vendedor, usuarioComprador: this.usuario});
    modal.present();
  }

 openLaunchNatigator(vendedor) {
  let options: LaunchNavigatorOptions = {
    start: [this.usuario.localizacao.latitude,this.usuario.localizacao.longitude],
    app: this.launch.APP.GOOGLE_MAPS
  };

  this.launch.navigate([vendedor.localizacao.latitude,vendedor.localizacao.longitude], options)
  .then(
    success => console.log('Launched navigator'),
    error => alert('Error launching navigator' + error)
  );
}  
 


pageListFavoritos() {
  this.navCtrl.push(ListFavoritosPage, {usuario: this.usuario});
}

//---------------------------------------------------


adicionarListFavoritos(vendedor: any) {
  console.log(vendedor.vendedor);
  let vend = new Favoritos;
  vend.id = vendedor.vendedor.id;
  vend.nome = vendedor.vendedor.pessoa.nomeCompleto;
  vend.hasOnline = false;
  vend.hasPerto = false;
  if(vendedor.vendedor.imagem) {
    vend.urlImg = vendedor.vendedor.imagem.url;
  } else { vend.urlImg = 'https://firebasestorage.googleapis.com/v0/b/meuprojeto-cde05.appspot.com/o/user.png?alt=media&token=8e00fe7b-8383-47a8-8a94-8e09dc476c85'}
  vend.online = vendedor.vendedor.online;
  vend.token = vendedor.vendedor.token ? vendedor.vendedor.token : 'token1213123';
  this.favService.adicionarListaFavoritos(vend).catch(error => console.error(error))
  .then(() => {
    this.toastCtrl.create({duration: 3000, message: `${vendedor.vendedor.pessoa.nomeCompleto} foi adicionado aos favoritos.`}).present();
    vendedor.hasFav = true;
  })
  //this.listFavoritos.push(vend);
}

removerListFavoritos(vendedor: any) {
  console.log(vendedor.vendedor);
  this.favService.removerListaFavoritos(vendedor.vendedor.id).get().then(favs => {
    favs.docs.forEach(fav => {
      if(fav.data().id == vendedor.vendedor.id) {
        fav.ref.delete().then(() => {
          this.toastCtrl.create({message: `${vendedor.vendedor.pessoa.nomeCompleto} foi removido dos favoritos`, duration: 3000}).present();
            vendedor.hasFav = false;
        })
      }
    })
  })
}


loadMap(position: Geoposition) {

  
  // Wait the MAP_READY before using any methods.
 
       this.map.addMarker({
         title: 'Você esta aqui!',
         icon: 'blue',
         position: {
           lat: position.coords.latitude,
           lng: position.coords.longitude
          }
        })
       .then((marker: Marker) => {
        console.log(marker);
        //this.markerVendedor = marker;
         
         
       });
       
       this.markerVendedores();
     
 }

 markerVendedores() {
  
     this.usuarioService.buscarCompradoresOnlinePerto(this.usuario.localizacao.cidade).take(1).toPromise().then(comps => {
      if(comps.length > 0 ) {
        comps.forEach(comp => {
         
          console.log(comp);
          
            this.map.addMarker({
            
            icon: 'green',
           
            position: {
             lat: comp.localizacao.latitude,
             lng: comp.localizacao.longitude
            }
          })
          .then((marker: Marker) => {
            console.log(marker);
            this.markers.push(marker);
            
            marker.one(GoogleMapsEvent.MARKER_CLICK)
              .then(() => {
                let compradorDistance = this.verDistanceMatrixIndividual(this.usuario, comp);
                this.compradorView = compradorDistance;
                console.log(this.compradorView);
              });
              
          });
          
        });
      }
      
     })
    
 }

 messageToComprador() {
  this.modalCtrl.create(MessageToCompradorPage, {vendedor: this.usuario, comprador: this.compradorView}).present();
  this.mService.adicionarIdChat(this.compradorView.vendedor.id, this.compradorView.vendedor.pessoa.nomeCompleto)
  .then(() => 
    this.mService.adicionarIdChatDestinatario(this.usuario.id, this.usuario.pessoa.nomeCompleto, this.compradorView.vendedor.id)
  )
  .catch(error => console.error(error));
}

 messageToVendedor(vendedor) {
  this.modalCtrl.create(MessageToCompradorPage, {vendedor: vendedor, comprador: this.usuario}).present();
  this.mService.adicionarIdChat(vendedor.vendedor.id, vendedor.vendedor.pessoa.nomeCompleto)
  .then(() => 
  this.mService.adicionarIdChatDestinatario(this.usuario.id, this.usuario.pessoa.nomeCompleto, vendedor.vendedor.id)
  )
  .catch(error => console.error(error));

}

 apagarMarkersCompradores() {
   for (let index = 0; index < this.markers.length; index++) {
      this.markers[index].remove();
     
   }
   this.markers = [];
 }

pageMessages() {
  this.navCtrl.push(ListMessagesPage, {usuario: this.usuario});
} 

deslogar() {
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
    this.usuarioService.sair().then(() => {
      let online = false;
      this.usuarioService.atualizarStatus(online, this.id).catch(error => console.log(error))
      .then(() => {
        if(this.usuario.pessoa.tipoPessoa == 'vendedor') {
          this.usuarioService.avisarComprador(this.id, online);
        }
      });
      toast.setMessage('Deslogado com sucesso.');
      toast.present();
      this.subscription.unsubscribe();
      this.navCtrl.setRoot(HomePage);
    
    })
}


}
