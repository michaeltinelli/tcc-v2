import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Notificacao } from '../../model/model';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class NotificadorProvider {
  notificacoesCollection: AngularFirestoreCollection<Notificacao>;
  notificacoes: Observable<Notificacao[]>;
  pathNotificacao = "notificacao";

  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
   this.notificacoesCollection = this.afs.collection<Notificacao>('notificacao');
   this.notificacoes = this.notificacoesCollection.valueChanges();
  }
  //this.afh.auth.currentUser.uid
  //const objRef = this.afs.collection(`${this.pathUsuarios}`).doc(comp.id).collection('favoritos');
 
  //firebase.firestore.FieldValue.delete()
  
 


}
