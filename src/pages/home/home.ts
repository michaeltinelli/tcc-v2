import { LonginProvider } from './../../providers/longin/longin';
import { CadastroPage } from './../cadastro/cadastro';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InicialUsuarioPage } from '../inicial-usuario/inicial-usuario';
import { AngularFireAuth } from 'angularfire2/auth';
import { RecuperarContaPage } from '../recuperar-conta/recuperar-conta';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  form: FormGroup;
  email: string;
  senha: string;

  constructor(
    public navCtrl: NavController,
    private loginService: LonginProvider,
    private toastCrl: ToastController,
    private afAuth: AngularFireAuth
  ) {
    afAuth.authState.subscribe(user => {
      if(user){ 
        this.navCtrl.setRoot(InicialUsuarioPage, {id: user.uid})
      }
      
    });
    this.createForm();
  }

  pageCadastro() {
    this.navCtrl.push(CadastroPage);
  }

  createForm() {
    this.form = new FormGroup({
      email: new FormControl('',[ 
        Validators.required
      ]),
      senha: new FormControl('',[
        Validators.minLength(5),
        Validators.required,
        Validators.maxLength(15)
      ])
    });
  }

  logar()  {

    if(this.form.valid) {
     
     this.email = this.form.value.email;

     //encoda a senha
     this.senha = btoa(this.form.value.senha);
     
     let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
      
      this.loginService.login(this.email, this.senha)
        .then((u: any) => {
          let id = u.uid;
         
          toast.setMessage('Logado com sucesso!');
          toast.present();
          this.navCtrl.setRoot(InicialUsuarioPage, {id: id});
        })
        .catch((error: any) => {
          console.log(error);
          if (error.code  == 'auth/email-already-in-use') {
            toast.setMessage('O e-mail digitado já está em uso.');
          } else if (error.code  == 'auth/invalid-email') {
            toast.setMessage('O e-mail digitado não é valido.');
          } else if (error.code  == 'auth/operation-not-allowed') {
            toast.setMessage('Não está habilitado criar usuários.');
          } else if (error.code  == 'auth/weak-password') {
            toast.setMessage('A senha digitada é muito fraca.');
          } else if (error.code  == 'auth/wrong-password') {
            toast.setMessage('Senha incorreta.');
          } else if (error.code  == 'auth/user-not-found') {
            toast.setMessage('Usuário não encontrado.');
          }
          toast.present();
        });
    }
    
  }

  recuperarContaPage() {
    this.navCtrl.push(RecuperarContaPage);
  }
}
