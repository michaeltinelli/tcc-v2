import { Component } from '@angular/core';
import { Platform, NavController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { HomePage } from '../pages/home/home';
import { AngularFireAuth } from 'angularfire2/auth';
import { InicialUsuarioPage } from '../pages/inicial-usuario/inicial-usuario';
import { BackgroundMode } from '@ionic-native/background-mode';
import { UsuarioProvider } from '../providers/usuario/usuario';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tap } from 'rxjs/operators';
import { Firebase } from '@ionic-native/firebase';

declare var cordova;

//AIzaSyDY8IF7Cmq0mCW7bKtv41t-1v4u2QdqZMc
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  rootPageParams: any;

  constructor(platform: Platform, statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    private backgroundMode: BackgroundMode,
    private uService: UsuarioProvider,
    private toastCtrl: ToastController,
    private firebaseNative: Firebase,
  ) {
    platform.ready().then(() => {
      //cordova.plugins.backgroundMode.setEnabled(true);
      //cordova.plugins.backgroundMode.moveToForeground();
     
      statusBar.styleDefault();
      splashScreen.hide();
    });
   
    
  }
}

