import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { Message, User } from '../../model/model';


@Injectable()
export class MessagesProvider {

  usuariosCollection = this.afs.collection<User>('usuarios');
  usuarioDocument = this.usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
  chatIdsCollection = this.usuarioDocument.collection('chatsIds');

  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    //this.afh.auth.currentUser.uid
  }

  getCollectionChat(id) {
    return this.usuarioDocument.collection(`messages${id}`, ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.orderBy('dataMessage');
      return query;
    }).valueChanges();
  }

  getLastCollectionChat(id) {
    return this.usuarioDocument.collection(`messages${id}`, ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.orderBy('dataMessage','desc').limit(1);
      return query;
    }).valueChanges();
  }

  adicionarMensagemOrigin(message: Message) {
    const msgCollection = this.usuarioDocument.collection(`messages${message.idDestinatario}`);
    return msgCollection.add(JSON.parse(JSON.stringify(message)));
  }

  adicionarMensagemDestinatario(message: Message) {
    const usuarioDocument = this.usuariosCollection.doc(`${message.idDestinatario}`);
    const msgCollection = usuarioDocument.collection(`messages${message.idRemetente}`);
    return msgCollection.add(JSON.parse(JSON.stringify(message)));
  }

  adicionarIdChat(id, nome) {
    return this.chatIdsCollection.doc(id).set({id: id, nome: nome});
  }
  
  adicionarIdChatDestinatario(id, nome, idu) {
    return this.usuariosCollection.doc(idu).collection('chatsIds').doc(id).set({id: id, nome: nome});
  }

  getChatDocs() {
    return this.chatIdsCollection.valueChanges();
  }
  
}
