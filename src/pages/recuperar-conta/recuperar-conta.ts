import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LonginProvider } from '../../providers/longin/longin';

@IonicPage()
@Component({
  selector: 'page-recuperar-conta',
  templateUrl: 'recuperar-conta.html',
})
export class RecuperarContaPage {

  form: FormGroup;
  email: string;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCrl: ToastController,
    private loginService: LonginProvider,
  ) {
    this.createForm();
  }

  ionViewDidLoad() {
    
  }

  createForm() {
    this.form = new FormGroup({
      email: new FormControl('',[ 
        Validators.required,
        Validators.minLength(5),
      ])
    });
  }

 recuperarConta() {

  if(this.form.valid) {
    let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
    this.email = this.form.value.email;
    this.loginService.resetPassword(this.email)
    .then(() => {
    
      toast.setMessage('Sucesso. Verifique seu e-mail para trocar a senha.');
      toast.present();
      this.navCtrl.pop();
    })
    .catch((error: any) => {
    
      if (error.code  == 'auth/invalid-email') {
        toast.setMessage('O e-mail digitado não é valido.').present();
      } else  if (error.code  == 'auth/argument-error') {
        toast.setMessage('O e-mail digitado não é valido.').present();
      } else  if (error.code  == 'auth/user-not-found') {
        toast.setMessage('O e-mail digitado não é cadastrado.').present();
      }
      
      })
    }
  }  
}
