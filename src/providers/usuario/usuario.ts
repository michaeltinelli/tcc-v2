import { ImagemVenda, Venda, Localizacao, Distance, Favoritos, Endereco } from './../../model/model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { User, Imagem } from '../../model/model';
import * as firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { query } from '@angular/core/src/animation/dsl';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';
import { Platform, AlertController, ToastController, NavController } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Firebase } from '@ionic-native/firebase';
import { DocumentReference } from '@firebase/firestore-types';
import { ListFavoritosPage } from '../../pages/list-favoritos/list-favoritos';


declare var cordova;

@Injectable()
export class UsuarioProvider {
  
  comprador: User;
  pathUsuarios ='usuarios';
  pathImagens = 'imagens';
  pathMatrixDistance = "mDistance"
  imagem: any;
  isNotificou: boolean;
  imagensVenda: ImagemVenda[]=[];
  private itemsCollection: AngularFirestoreCollection<User>;
  items: Observable<User[]>;
   favs: User[];
   subscription: Subscription = new Subscription();
   
   private favoritosCollection: AngularFirestoreCollection<Favoritos>;
   private favoritos: Observable<Favoritos[]>;

  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
    private geolocation: Geolocation,
    private local: LocalNotifications,
    private platform: Platform,
    private alertCtrl: AlertController,
    private backgroundMode: BackgroundMode,
    private firebaseNative: Firebase,
    private toastCtrl: ToastController,
    
   
  ) {
   
      this.afh.authState.toPromise().then(user => {
        if (user != null) {
        
     // this.favoritos =  this.afs.collection<Favoritos>(`usuarios/${this.afh.auth.currentUser.uid}/favoritos`).valueChanges();
          
      }
    });
  }

  

  buscarDadosUsuario(id) {
    
    return this.afs.doc(`${this.pathUsuarios}/${id}`).snapshotChanges().map(v => {
      if(v.payload.exists) {

        const usuario = v.payload.data() as User;
            usuario.id = v.payload.id;
            return usuario;
      }
      else {
        return null;
      }
    });
   }

   buscarDadosUsuarioAlternative(id) {
    return this.afs.doc(`${this.pathUsuarios}/${id}`).snapshotChanges().map(v => {
      if(v.payload.exists) {

        const usuario = v.payload.data() as any;
            usuario.id = v.payload.id;
            return usuario;
      }
      else {
        return null;
      }
    });
   }
   

   atualizarDados(usuario: User){
     //this.db.object(`${this.pathUsuarios}/${usuario.id}`).set((JSON.parse(JSON.stringify(usuario))));
    return this.afs.doc(`${this.pathUsuarios}/${usuario.id}`).set((JSON.parse(JSON.stringify(usuario))));
   }

   atualizarStatus(value: boolean, id: any){
       return this.afs.doc(`${this.pathUsuarios}/${id}`).update({online: value})//.then(() => this.avisarComprador(id,value));
   }

   avisarComprador(id: string, value: boolean) {
    let usuariosCollection = this.afs.collection<User>('usuarios').ref.where('pessoa.tipoPessoa','==', 'comprador').get()
    .then(comps => {
      if(comps.docs.length > 0) {
        comps.forEach(comp => {
          let usuariosCollection = this.afs.collection<User>(this.pathUsuarios);
          let usuarioDocument = usuariosCollection.doc(comp.ref.id);
          let query = usuarioDocument.collection('favoritos');
          
          query.ref.get().then(favs => {
            //let fav = favs.docs.find(fav => fav.data().id == id).data();
            if(favs.docs.length > 0) {
              let mainid = favs.docs.find(fav => fav.data().id == id).ref.id;
              query.doc(mainid).update({online: JSON.parse(JSON.stringify(value))});
            }
          })
        })
      }
    })
   
   }

   atualizarDadosVenda(venda: Venda) {
     return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({venda: (JSON.parse(JSON.stringify(venda)))});
   }

   pegarLocalizacao() {
      return this.geolocation.watchPosition();
   }

   atualizarLocalizacao(location: any, endereco: any) {
    //this.db.object(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({localizacao: (JSON.parse(JSON.stringify(location)))});
    this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({endereco: (JSON.parse(JSON.stringify(endereco)))});
    return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({localizacao: (JSON.parse(JSON.stringify(location)))});
   }

   atualizarEndereco(endereco: Endereco) {
      return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({endereco: (JSON.parse(JSON.stringify(endereco)))});
   }

   vendedoresOnline(cidade: string, palavra) {
     return this.afs.collection<User>('usuarios', ref => 
       ref.where('online','==', true).where('pessoa.tipoPessoa','==','vendedor').where('localizacao.cidade','==', cidade)
      ).snapshotChanges()
      .map(changes => {
        return changes.map(v => {
            const data = v.payload.doc.data() as User;
            data.id =  v.payload.doc.id;
            
           return data;
        });
     });
   }

   vendedoresOnlineTodos() {
    return this.afs.collection<User>('usuarios', ref => 
      ref.where('pessoa.tipoPessoa','==','comprador')
     ).snapshotChanges()
     .map(changes => {
       return changes.map(v => {
           const data = v.payload.doc.data() as User;
           data.id =  v.payload.doc.id;
           
          return data;
       });
    }).toPromise();
  }

   pegarVendedoresOnline(cidade: string) {
     
     return this.afs.collection<User>('usuarios', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.where('pessoa.tipoPessoa','==','vendedor').where('online','==', true).where('endereco.cidade','==', cidade).orderBy('pessoa.nomeCompleto')
      return query;
    }).valueChanges()
  }

   salvarMdistance(distance: Distance) {
    return this.afs.doc(`${this.pathMatrixDistance}/${distance.idComprador}`).set((JSON.parse(JSON.stringify(distance))));
   }

  atualizarFavoritos(favoritos) {
    const fav = favoritos.map((obj)=> {return Object.assign({}, obj)});
    return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({favoritos: fav});
  }
  
  atualizarCompradorFavoritos(favoritos, id) {
    const fav = favoritos.map((obj)=> {return Object.assign({}, obj)});
    return this.afs.doc(`${this.pathUsuarios}/${id}`).update({favoritos: fav});
  }

  pegarVendedorFavorito(id) {
    return this.afs.doc(`${this.pathUsuarios}/${id}`).valueChanges();
  
  } 

  pegarVendedoresFavoritos(favoritos: Favoritos[]) {
    let array: Observable<User[]>;
    favoritos.forEach(fav => {
      
      array = this.afs.collection<User>('usuarios', ref => {
        let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        query = query.where('pessoa.tipoPessoa','==','vendedor').where('id','==', fav.id).orderBy('pessoa.nomeCompleto')
        return query;
      }).valueChanges()
    })
    return array;
  } 

  atualizarFavoritosHasOnline(usuario: User) {
    return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({favoritos: usuario.favoritos });
  }


  notificarComprador(usuario: User) {
    //console.log(usuario);
    this.afs.collection<User>('usuarios', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.where('pessoa.tipoPessoa','==','comprador').where('online','==', true)//.where('localizacao.cidade','==', usuario.localizacao.cidade)
      return query;
    }).valueChanges().subscribe(comps => {
      console.log(comps);
      comps.forEach(comprador => {
        console.log(comprador);
        let comp = comprador.favoritos.find(fav => fav.id == usuario.id);
        if (comp) {
         // if(comp.hasOnline == true) { this.notificarCompradorVendhasOnline(usuario);}
        }
        /*
        if (comprador.favoritos.find(fav => fav.id == usuario.id)) {
          comprador.favoritos.forEach(fav => {
            console.log(fav);
            if (fav.hasOnline ==  true) {
              this.notificarCompradorVendhasOnline(usuario);
            }
          })
        } 
        */
      })
    })
     
  }

  notificarCompradorVendhasOnline(usuario: User) {
    
    this.platform.ready().then(() => {
      cordova.plugins.notification.local.schedule(({
        icon: usuario.imagem.url,
        title: `${usuario.pessoa.nomeCompleto} está online.`,
        text: `Acesse o menu favoritos.`,
        data: { vend: usuario },
        trigger: { at: new Date(new Date().getTime() + 4000)},
        foreground: false
      }));
      
    });
  }

  
  async pegarToken() {
    let token;

   
    if (this.platform.is('android')) {
     // await this.firebaseNative.grantPermission();
      token = await this.firebaseNative.getToken().catch(error => console.error(error));
      console.log(token);
    }

    if (this.platform.is('ios')) {
      token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
      console.log(token);
    }
    return this.salvarToken(token)
  }

  private salvarToken(token) {
    return this.afs.doc(`${this.pathUsuarios}/${this.afh.auth.currentUser.uid}`).update({token: (JSON.parse(JSON.stringify(token)))});
  }

  ouvirNotificacoes() {
    console.log('chamando?');
    return this.firebaseNative.onNotificationOpen();
  }

  buscarCompradoresOnlinePerto(cidade: string) {
    return this.afs.collection<User>('usuarios', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      query = query.where('pessoa.tipoPessoa','==','comprador').where('online','==', true).where('endereco.cidade','==', cidade).orderBy('pessoa.nomeCompleto')
      return query;
    }).valueChanges();
  }

  sair() {
    return this.afh.auth.signOut().then(()=> this.subscription.unsubscribe());
  }
}
