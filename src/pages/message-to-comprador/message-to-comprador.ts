import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { tap } from 'rxjs/operators';
import { MessagesProvider } from '../../providers/messages/messages';
import { Message } from '../../model/model';
import { UsuarioProvider } from '../../providers/usuario/usuario';

@IonicPage()
@Component({
  selector: 'page-message-to-comprador',
  templateUrl: 'message-to-comprador.html',
})
export class MessageToCompradorPage {
  comprador: any;
  vendedor: any;
  results: any[]=[];
  message: Message;
  messageChat = '';
  messages: any[];
  nome = '';
  id = '';
  id2 = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private stService: StorageProvider,
    private meService: MessagesProvider,
    private toastCtrl: ToastController,
    private uService: UsuarioProvider
  ) {
    
    this.vendedor = this.navParams.data.vendedor;
    this.comprador = this.navParams.data.comprador;
    console.log(this.vendedor);
    console.log(this.comprador);

    if(this.comprador.id && this.vendedor.id ) {
      this.id = this.comprador.id;
      this.id2 = this.vendedor.id;
      this.nome = this.vendedor.nome;
      this.meService.getCollectionChat(this.vendedor.id).subscribe((data) => {
        if(data.length > 0) {
          this.messages = [];
          this.messages = data;
        }
      })
    } else if(this.comprador.id && this.vendedor.vendedor ) {
      this.id = this.vendedor.vendedor.id;
      this.id2 = this.comprador.id;
      this.nome = this.vendedor.vendedor.pessoa.nomeCompleto;
      this.meService.getCollectionChat(this.vendedor.vendedor.id).subscribe((data) => {
        if(data.length > 0) {
          this.messages = [];
          this.messages = data;
        }
      })  
    } else if (this.comprador.vendedor && this.vendedor.id) {
      this.id = this.comprador.vendedor.id;
      this.id2 = this.vendedor.id;
      this.nome = this.comprador.vendedor.pessoa.nomeCompleto;
        this.meService.getCollectionChat(this.comprador.vendedor.id).subscribe((data) => {
          if(data.length > 0) {
            this.messages = [];
            this.messages = data;
          }
      }) 
    }   
   console.log(this.messages);
   this.messages = [];
  }

  ionViewDidLoad() {
    
  }

  enviarMessagem() {

    this.message = new Message;
    this.message.mensagem = this.messageChat;
    
    this.message.dataMessage = Date();
    
    

    //vendedor usando chat no comprador
    if (this.comprador.vendedor && this.vendedor.id) {
      this.message.idRemetente = this.vendedor.id;
      this.message.idDestinatario = this.comprador.vendedor.id;
      this.message.nome = this.vendedor.pessoa.nomeCompleto;
      this.message.token = this.comprador.vendedor.token;
      
    } else if(this.comprador.id && this.vendedor.vendedor ) {
      this.message.idRemetente = this.comprador.id;
      this.message.idDestinatario = this.vendedor.vendedor.id;
      this.message.nome = this.comprador.pessoa.nomeCompleto;
      this.message.token = this.vendedor.vendedor.token;
    } else if(this.comprador.id && this.vendedor.id ) {
      
      this.message.idRemetente = this.comprador.id;
      this.message.idDestinatario = this.vendedor.id;
      this.message.nome = this.comprador.pessoa.nomeCompleto;
      this.uService.buscarDadosUsuario(this.vendedor.id).take(1).toPromise().then(u => {
        this.message.token = u.token;
      })

    }

    
    this.meService.adicionarMensagemOrigin(this.message).then(() => {
      this.meService.adicionarMensagemDestinatario(this.message).catch(error => console.error(error))
        this.messageChat = '';
    })
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }
}
