import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../model/model';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';


@Injectable()
export class LonginProvider {

  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    
  }

  login(email: string, senha: string) {
    return this.afh.auth.signInWithEmailAndPassword(email, senha);
  }

  resetPassword(email: string) {
    return this.afh.auth.sendPasswordResetEmail(email);
  }
}
