import { EditarDadosPage } from './../pages/editar-dados/editar-dados';
import { InicialUsuarioPage } from './../pages/inicial-usuario/inicial-usuario';
import { CadastroPage } from './../pages/cadastro/cadastro';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, forwardRef } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FormsModule } from '@angular/forms';

import { Camera } from '@ionic-native/camera';

import { GoogleMaps, Geocoder } from '@ionic-native/google-maps';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { CadastroProvider } from '../providers/cadastro/cadastro';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';
import { LonginProvider } from '../providers/longin/longin';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { CadastroVendaPage } from '../pages/cadastro-venda/cadastro-venda';
import { StorageProvider } from '../providers/storage/storage';
import { ImagePicker } from '@ionic-native/image-picker';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { InformacoesUsuarioPage } from '../pages/informacoes-usuario/informacoes-usuario';
import { Geolocation } from '@ionic-native/geolocation';
import { InformacoesVendaPage } from '../pages/informacoes-venda/informacoes-venda';
import { OrderModule, OrderPipe } from 'ngx-order-pipe';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { ListFavoritosPage } from '../pages/list-favoritos/list-favoritos';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { NotificadorProvider } from '../providers/notificador/notificador';
import { BackgroundMode } from '@ionic-native/background-mode';


import { FavoritosProvider } from '../providers/favoritos/favoritos';
import { Http } from '@angular/http';
import { OpcoesFavoritoPage } from '../pages/opcoes-favorito/opcoes-favorito';
import { RecuperarContaPage } from '../pages/recuperar-conta/recuperar-conta';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { MessagesProvider } from '../providers/messages/messages';
import { MessageToCompradorPage } from '../pages/message-to-comprador/message-to-comprador';
import { ListMessagesPage } from '../pages/list-messages/list-messages';
import { Firebase } from '@ionic-native/firebase';


export const firebaseConfig = {
  apiKey: "AIzaSyA25xauLKk167TPlYJ1bJwCTIyFODfeisc",
    authDomain: "meuprojeto-cde05.firebaseapp.com",
    databaseURL: "https://meuprojeto-cde05.firebaseio.com",
    projectId: "meuprojeto-cde05",
    storageBucket: "meuprojeto-cde05.appspot.com",
    messagingSenderId: "13396848938"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastroPage,
    InicialUsuarioPage,
    EditarDadosPage,
    CadastroVendaPage,
    InformacoesUsuarioPage,
    InformacoesVendaPage,
    ListFavoritosPage,
    OpcoesFavoritoPage,
    RecuperarContaPage,
    MessageToCompradorPage,
    ListMessagesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    IonicImageViewerModule,
    OrderModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastroPage,
    InicialUsuarioPage,
    EditarDadosPage,
    CadastroVendaPage,
    InformacoesUsuarioPage,
    InformacoesVendaPage,
    ListFavoritosPage,
    OpcoesFavoritoPage,
    RecuperarContaPage,
    MessageToCompradorPage,
    ListMessagesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CadastroProvider,
    LonginProvider,
    UsuarioProvider,
    Camera,
     Geolocation,
    StorageProvider,
    GoogleMaps,
    Geocoder,
    LaunchNavigator,
    NativeGeocoder,
    OrderPipe,
    CallNumber,
    LocalNotifications,
    NotificadorProvider,
    BackgroundMode,
    
    Firebase,
    FavoritosProvider,
    BackgroundGeolocation,
    MessagesProvider,
  ]
})
export class AppModule {}
