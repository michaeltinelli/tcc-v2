import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Favoritos, User } from '../../model/model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { tap } from 'rxjs/operators';
import { UsuarioProvider } from '../usuario/usuario';

@Injectable()
export class FavoritosProvider {

  private favoritosCollection: AngularFirestoreCollection<Favoritos>;
  private favoritos: Observable<Favoritos[]>;
  constructor(
    private afh: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    this.afh.authState.take(1).toPromise().then(user => {
      if (user != null) {
        console.log(user);
      // this.favoritosCollection =  this.afs.collection('/usuarios').doc(this.afh.auth.currentUser.uid).collection<Favoritos>('/favoritos');
       /* 
       this.favoritos = this.favoritosCollection.snapshotChanges().map(actions => {
          return actions.map(action => {
            const favorito = action.payload.doc.data() as Favoritos;
            favorito.id = action.payload.doc.id;
            return favorito;
          })
        })//.toPromise().then(dados => console.log(dados)).catch(error => console.error(error))
        */
      
        //this.favoritosCollection.valueChanges().subscribe(dados => console.log(dados));
      }
    }).catch(error =>console.error(error));
  
  }

  getListaFavoritos() {
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos');
    return query;
  }

  getListaFavoritosPorId(id) {
    console.log(id);
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos').ref.where("id", "==", id);
    return query;
  }

  adicionarListaFavoritos(fav: Favoritos) {
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos');
     return query.add(JSON.parse(JSON.stringify(fav)));
  }
   
  removerListaFavoritos(id: string) {
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos').ref.where("id", "==", id);
    return query;
  }

  updadeHasOnline(dados) {
    let id: string;
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos')
    
    .ref.where("id","==", dados.id).get().then(favs => {
      id = favs.docs.find(fav => fav.data().id == dados.id).id;
      // id = mainId;
      return usuarioDocument.collection('favoritos').doc(id).update({hasOnline: dados.hasOnline}); 
    });
    
    return query;
    
  }

  updadeHasPerto(dados) {
    let id: string;
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos')
    
    .ref.where("id","==", dados.id).get().then(favs => {
      id = favs.docs.find(fav => fav.data().id == dados.id).id;
      // id = mainId;
      return usuarioDocument.collection('favoritos').doc(id).update({hasPerto: dados.hasPerto}); 
    });
    
    return query;
    
  }

  removerDosFavoritos(favorito) {
    console.log(favorito);
    let id: string;
    const usuariosCollection = this.afs.collection<User>('usuarios');
    const usuarioDocument = usuariosCollection.doc(`${this.afh.auth.currentUser.uid}`);
    const query = usuarioDocument.collection('favoritos')
    
    .ref.where("id","==", favorito.id).get().then(favs => {
      id = favs.docs.find(fav => fav.data().id == favorito.id).id;
      // id = mainId;
      return usuarioDocument.collection('favoritos').doc(id).delete(); 
    });
    
    return query;
    
  }

 
}
