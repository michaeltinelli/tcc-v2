import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarDadosPage } from './editar-dados';

@NgModule({
  declarations: [
    EditarDadosPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarDadosPage),
  ],
})
export class EditarDadosPageModule {}
