import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InicialUsuarioPage } from './inicial-usuario';

@NgModule({
  declarations: [
    InicialUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(InicialUsuarioPage),
  ],
})
export class InicialUsuarioPageModule {}
