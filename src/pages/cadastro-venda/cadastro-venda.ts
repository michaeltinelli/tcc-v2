import { ImagemVenda, Venda } from './../../model/model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Imagem, User } from '../../model/model';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { StorageProvider } from '../../providers/storage/storage';
import { Camera } from '@ionic-native/camera';
import { tap } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-cadastro-venda',
  templateUrl: 'cadastro-venda.html',
})
export class CadastroVendaPage {
  id = '';
  form: FormGroup;
  usuario: User;
  imagensVenda: ImagemVenda[]=[];

  photos: any[]=[];
  results: any[]=[];
  error: any;

  plChaves = [];
  palavra = '';

  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
     private usuarioService: UsuarioProvider,
     private toastCrl: ToastController,
     private stService: StorageProvider,
     private camera: Camera,
     private alertCtrl: AlertController
    ) {
      this.usuario = this.navParams.data.usuario;
      if (!this.usuario.venda) {
        this.usuario.venda = new Venda;
      }
      if (this.usuario.venda.palavrasChave) {
        this.plChaves = this.usuario.venda.palavrasChave;
      }
      this.createForm(this.usuario);
      this.stService.buscarImagens().subscribe((imgs) => {
        if (imgs.length > 0) {
          this.results = imgs;
        }
        if (imgs.length == 0) {
          this.results = [];
        }
      })
      /*
      this.usuarioService.ouvirNotificacoes().pipe(
        tap(msg => {
          console.log(msg);
          this.toastCrl.create({
            
            message: `${msg.icon}  ${msg.title}`,
            duration: 3000
          }).present();
          //this.navCtrl.push(ListFavoritosPage, {usuario: this.usuario});
        })
      ).subscribe(() => {
      });
      */
  }

  ionViewDidLoad() {
    
  }

  createForm(usuario: User) {
    console.log(usuario);
  
    if (usuario.venda) {
      this.form = new FormGroup({
        descricao: new FormControl(usuario.venda.descricao,[
          Validators.minLength(10),
          Validators.required,
          Validators.maxLength(200)
        ]),
        
        /*
        palavrasChave: new FormControl('',[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(15)
        ]),
        */
        
        titulo: new FormControl(usuario.venda.titulo,[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(35)
        ]),
      });
    }
    else {
      this.form = new FormGroup({
        descricao: new FormControl('',[
          Validators.minLength(10),
          Validators.required,
          Validators.maxLength(200)
        ]),
        /*
        palavrasChave: new FormControl('',[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(15)
        ]),
        */
        titulo: new FormControl('',[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(35)
        ]),
      });
    }

  }
  
  

  pegarPhotosGallery() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 450,
      targetWidth: 450,
      allowEdit: true
    })
    .then((imageData) => {
      let img: ImagemVenda;
      img = new ImagemVenda; 
      img.file = imageData;
      this.stService.salvarImagemVendaStorage(img);
    })
  }

  deletePhoto(imagem: ImagemVenda) {
   
    let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
     let confirm = this.alertCtrl.create({
       title: "Você tem certeza?",
       message: "Ao confirmar a imagem será deletada <strong>Permanentemente.</strong>",
       buttons: [
         {
           text: "Não"
         }, 
         {
           text: "Sim tenho certeza!",
           handler: () => {
             this.stService.apagarFotoVendaDB(imagem).then(() => {
               toast.setMessage('Imagem excluída.');
               this.stService.apagarFotoVendaStorage(imagem.fullPath);
               this.stService.buscarImagens().subscribe((imgs) => {
                if (imgs.length > 0) {
                  this.results = imgs;
                }
                if (imgs.length == 0) {
                  this.results = [];
                }
              })
             }).catch(error => {
               toast.setMessage('Erro ao excluir.');
             })
           }
         }
       ]
     })
     confirm.present();
     toast.present();
  }


  adicionarPalavra() {
    console.log(this.palavra);
    this.plChaves.push(this.palavra);
    this.palavra = '';
  }

  deletarPalavra(index){
    this.plChaves.splice(index, 1);
  }

  salvar() {
    if(this.form.valid) {
     
      this.usuario.venda.descricao = this.form.value.descricao;
      this.usuario.venda.titulo = this.form.value.titulo;
      this.usuario.venda.palavrasChave = this.plChaves;
      let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
      this.usuarioService.atualizarDadosVenda(this.usuario.venda)
      .then(() => {
        toast.setMessage('Dados salvos com sucesso.');
        toast.present();
        this.navCtrl.pop();
      })
      .catch((error) => {
        toast.setMessage('Erro! ' + error);
        toast.present();
      }) 
     }
     
   }
}
