import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import {FirebaseApp} from 'angularfire2';
import { Imagem, ImagemVenda } from '../../model/model';

@Injectable()
export class StorageProvider {
  pathVendaFs = 'imagensVendas';
  pathStorageVendas = 'imagens/vendas';
  pathStoragePerfil = 'imagens/perfil';
  id: any;

  constructor(
    private afs: AngularFirestore,
    private afh: AngularFireAuth,
    private fb: FirebaseApp  
  ) {
    this.afh.authState.subscribe(user => {
      if (user != null) {
        //console.log(user);
        this.id = user.uid;
      }
    })

  }

  salvarImagemStorage(imagem: Imagem) {
    let storageRef = this.fb.storage().ref();
    imagem.fullPath = this.pathStoragePerfil + '/' + this.afh.auth.currentUser.uid + '.jpeg';
    let uploadTask = storageRef.child(imagem.fullPath).putString(imagem.file, 'base64');

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: firebase.storage.UploadTaskSnapshot) => {
        imagem.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error) => {
        console.error(error);
      },
      () => {
        imagem.url = uploadTask.snapshot.downloadURL;
        this.salvarImagemDB(imagem)
        return imagem;
      }
    )
     
  }

  private salvarImagemDB(imagem: Imagem){
     return this.afs.doc(`usuarios/${this.afh.auth.currentUser.uid}`).update({imagem: JSON.parse(JSON.stringify(imagem))});
  }

  apagarFotoPerfilDB(imagem: Imagem) {
    return this.afs.doc(`usuarios/${this.afh.auth.currentUser.uid}`).update({imagem: firebase.firestore.FieldValue.delete()})
  }

  public apagarFotoPerfilStorage(fullPath: string) {
    let storageRef = this.fb.storage().ref();
    storageRef.child(fullPath).delete();
  }

  ///-------------------------------------------- Any Imagens----------------------------------
  //mexer ainda
  salvarImagemVendaStorage(imagem: ImagemVenda) {
    imagem.id = this.afh.auth.currentUser.uid;
    let text = "";
    const possible = "ABCDEFGHIJKLMNO0123456789";

    for (var i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    imagem.name = text;
    let storageRef = this.fb.storage().ref();
    imagem.fullPath = this.pathStorageVendas + '/' + this.afh.auth.currentUser.uid + text + '.jpeg';
    let uploadTask = storageRef.child(imagem.fullPath).putString(imagem.file, 'base64');

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: firebase.storage.UploadTaskSnapshot) => {
        imagem.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error) => {
        console.error(error);
      },
      () => {
        imagem.url = uploadTask.snapshot.downloadURL;
       this.salvarImagemVendaDB(imagem);
      }
    )
     
  }

  private salvarImagemVendaDB(imagem: Imagem){
    return this.afs.doc(`${this.pathVendaFs}/${imagem.name}`).set(JSON.parse(JSON.stringify(imagem)));
 }

 apagarFotoVendaDB(imagem: Imagem) {
  return this.afs.doc(`${this.pathVendaFs}/${imagem.name}`).delete();
}

  public apagarFotoVendaStorage(fullPath: string) {
    let storageRef = this.fb.storage().ref();
    storageRef.child(fullPath).delete();
  }

  buscarImagens() {
    return this.afs.collection<ImagemVenda>(`${this.pathVendaFs}/`, ref => 
    ref.where('id','==',this.afh.auth.currentUser.uid)).valueChanges();
  }

  buscarImagensVendedor(id) {
    return this.afs.collection<ImagemVenda>(`${this.pathVendaFs}/`, ref => 
    ref.where('id','==',id)).valueChanges();
  }
}
