import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { MessagesProvider } from '../../providers/messages/messages';
import { Message, User } from '../../model/model';
import 'rxjs/add/operator/take';
import { MessageToCompradorPage } from '../message-to-comprador/message-to-comprador';

@IonicPage()
@Component({
  selector: 'page-list-messages',
  templateUrl: 'list-messages.html',
})
export class ListMessagesPage {
  messages: any[];
  usuario: User;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private mService: MessagesProvider,
  ) {
    this.usuario = this.navParams.data.usuario;

    this.mService.getChatDocs().take(1).toPromise().then(data => {
      this.messages = [];
      data.forEach((ch: any) => {
        this.mService.getLastCollectionChat(ch.id).subscribe((msg: Message[]) => {
          let index = this.messages.find(message => message.id === ch.id );
          this.messages.splice(index, 1);
          msg.forEach(ms => {
            console.log(ch);
            let chat = { id: '' ,nome: '', nomeChat: '', mensagem: '', data: undefined};
            chat.id = ch.id;
            chat.nome = ch.nome;
            if(ms.nome === this.usuario.pessoa.nomeCompleto) {
              chat.nomeChat = 'Você';
            }else { chat.nomeChat = ms.nome }
            if(ms.mensagem.length > 20) { 
              let aux = ms.mensagem.substring(0,20);
              chat.mensagem = aux.concat('...');  
            } else { chat.mensagem = ms.mensagem }
            chat.data = ms.dataMessage;
            this.messages.push(chat);
          });
        })
      })
    })
    console.log(this.messages);
  }

  ionViewDidLoad() {
    
  }

  pageChat(msg: any) {
    console.log(msg);
    this.navCtrl.push(MessageToCompradorPage, {comprador: this.usuario, vendedor: msg});
  }
}
