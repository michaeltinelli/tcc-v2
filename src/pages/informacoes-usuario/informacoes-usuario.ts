import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';
import { User } from '../../model/model';
import { StorageProvider } from '../../providers/storage/storage';
import { CallNumber } from '@ionic-native/call-number';

 declare var google: any;

@IonicPage()
@Component({
  selector: 'page-informacoes-usuario',
  templateUrl: 'informacoes-usuario.html',
})
export class InformacoesUsuarioPage {

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  usuarioVendedor: any;
  usuarioComprador: User;
  //results: any[]=[];
  error: any;
  key: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private stService: StorageProvider,
    private callNumber: CallNumber,
    private platform: Platform
  ) {
    this.usuarioComprador = this.navParams.data.usuarioComprador;
    this.usuarioVendedor = this.navParams.data.usuarioVendedor;
    console.log(this.usuarioComprador);
    console.log(this.usuarioVendedor);
  }

  ionViewDidLoad() {
    
  }

  ligarCelular(numero){
    this.callNumber.callNumber(numero, true)
    .catch(error => {
      alert("Erro ao telefonar"+ error)
    })
  }

  ligarCelularOpcional(numero){
    this.callNumber.callNumber(numero, true)
    .catch(error => {
      alert("Erro ao telefonar"+ error)
    })
  }

  
  dismiss(){
    this.viewCtrl.dismiss();
  }
}
