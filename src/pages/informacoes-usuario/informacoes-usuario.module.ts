import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformacoesUsuarioPage } from './informacoes-usuario';

@NgModule({
  declarations: [
    InformacoesUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(InformacoesUsuarioPage),
  ],
})
export class InformacoesUsuarioPageModule {}
