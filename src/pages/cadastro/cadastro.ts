import { HomePage } from './../home/home';
import { CadastroProvider } from './../../providers/cadastro/cadastro';
import { Imagem, User, Pessoa, Contato } from './../../model/model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {
  form: FormGroup;
  isVendedor = false;
  
 
  imagem: Imagem;
  usuario = new User();
  pessoa = new Pessoa();
  contato = new Contato();

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private cadService: CadastroProvider,
    private toastCrl: ToastController
  ) {
    this.createForm();

  }

  ionViewDidLoad() {
    
  }

  

  createForm() {
    if (this.isVendedor == true) {
      
      this.form = new FormGroup({
        nomeCompleto: new FormControl('',[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(40)
        ]),
        
        celular: new FormControl('',[
          Validators.pattern('[0-9]+'),
          Validators.minLength(10),
          Validators.required,
          Validators.maxLength(11),
          
        ]),
        celular2: new FormControl('',[
          Validators.pattern('[0-9]+'),
          Validators.minLength(10),
          Validators.maxLength(11)
        ]),
  
        email: new FormControl('',[ 
          Validators.required
        ]),
  
        senha: new FormControl('',[
          Validators.minLength(5),
          Validators.required,
          Validators.maxLength(15)
        ])
      });
    }
    else {
      this.form = new FormGroup({
        nomeCompleto: new FormControl('',[
          Validators.minLength(3),
          Validators.required,
          Validators.maxLength(40)
        ]),
  
        email: new FormControl('',[ 
          Validators.required
        ]),
  
        senha: new FormControl('',[
          Validators.minLength(5),
          Validators.required,
          Validators.maxLength(15)
        ])
      });
    }
    
  }

  criarConta()  {
    if(this.form.valid) {
     
     this.pessoa.nomeCompleto = this.form.value.nomeCompleto;
     this.usuario.email = this.form.value.email;

     //encoda a senha
     this.usuario.senha = btoa(this.form.value.senha);
     
     this.usuario.online = false;

      if(this.isVendedor == true) {
       
      
        this.contato.celular = this.form.value.celular;
        this.contato.celular2 = this.form.value.celular2;
        this.usuario.contato = this.contato;
        this.pessoa.tipoPessoa = 'vendedor';
      } else {
        this.pessoa.tipoPessoa = 'comprador';

      }
      
      this.usuario.pessoa = this.pessoa;
      

      let toast = this.toastCrl.create({ duration: 3000, position: 'bottom' });
      console.log(this.usuario);

      this.cadService.criarUsuario(this.usuario)
        .then((u: any) => {
          this.usuario.id = u.uid;
          this.cadService.cadastrarUsuario(this.usuario).catch((error) => {
            console.log(error);
          })
          u.sendEmailVerification();
          toast.setMessage('Conta criada com sucesso. Efetue o login.');
          
          toast.present();
          this.navCtrl.setRoot(HomePage);
          
        })
        .catch((error: any) => {
          if (error.code  == 'auth/email-already-in-use') {
            toast.setMessage('O e-mail digitado já está em uso.');
          } else if (error.code  == 'auth/invalid-email') {
            toast.setMessage('O e-mail digitado não é valido.');
          } else if (error.code  == 'auth/operation-not-allowed') {
            toast.setMessage('Não está habilitado criar usuários.');
          } else if (error.code  == 'auth/weak-password') {
            toast.setMessage('A senha digitada é muito fraca.');
          }
          toast.present();
        });
    }
    
  }
}
