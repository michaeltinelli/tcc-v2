import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpcoesFavoritoPage } from './opcoes-favorito';

@NgModule({
  declarations: [
    OpcoesFavoritoPage,
  ],
  imports: [
    IonicPageModule.forChild(OpcoesFavoritoPage),
  ],
})
export class OpcoesFavoritoPageModule {}
