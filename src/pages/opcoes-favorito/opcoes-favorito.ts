import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { User } from '../../model/model';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';


@IonicPage()
@Component({
  selector: 'page-opcoes-favorito',
  templateUrl: 'opcoes-favorito.html',
})
export class OpcoesFavoritoPage {
  favorito: any;
  usuario: User;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private toastCrl: ToastController,
    private favService: FavoritosProvider
  ) {
    this.favorito = this.navParams.data.favorito;
    this.usuario = this.navParams.data.usuario;
    console.log(this.favorito);
    console.log(this.usuario);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpcoesFavoritoPage');
  }

  changeNotificarOnline(dados) {
    dados.hasOnline = !dados.hasOnline;
    console.log(dados);
    this.favService.updadeHasOnline(dados).then(() => this.toastCrl.create({message: 'Alteração feita com sucesso', duration: 4000, position: 'top'}).present())
    .catch(() => this.toastCrl.create({message: 'Erro ao trocar. Tente novamente mais tarde.', duration: 4000, position: 'bottom'}).present())
  }

  changeNotificarPerto(dados) {
    dados.hasPerto = !dados.hasPerto;
    console.log(dados);
    this.favService.updadeHasPerto(dados).then(() => this.toastCrl.create({message: 'Alteração feita com sucesso', duration: 4000, position: 'top'}).present())
    .catch(() => this.toastCrl.create({message: 'Erro ao trocar. Tente novamente mais tarde.', duration: 4000, position: 'bottom'}).present())
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
